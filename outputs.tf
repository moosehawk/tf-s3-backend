output "primary_region" {
  description = "Primary region where tfstate is located."
  value       = data.aws_region.primary.name
}

output "secondary_region" {
  description = "Secondary region where replicated tfstate is located."
  value       = data.aws_region.secondary.name
}

output "s3_primary_bucket_id" {
  description = "ID of the primary tfstate S3 bucket."
  value       = aws_s3_bucket.primary.id
}

output "s3_primary_bucket_arn" {
  description = "ARN of the primary tfstate S3 bucket."
  value       = aws_s3_bucket.primary.arn
}

output "s3_secondary_bucket_id" {
  description = "ID of the secondary tfstate S3 bucket."
  value       = aws_s3_bucket.secondary.id
}

output "s3_secondary_bucket_arn" {
  description = "ARN of the secondary tfstate S3 bucket."
  value       = aws_s3_bucket.secondary.arn
}

output "dynamodb_lock_table_id" {
  description = "ID of the DynamoDB tfstate lock table."
  value       = aws_dynamodb_table.this.id
}

output "dynamodb_lock_table_arn" {
  description = "ARN of the DynamoDB tfstate lock table."
  value       = aws_dynamodb_table.this.arn
}
