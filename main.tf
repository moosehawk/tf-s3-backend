data "aws_caller_identity" "current" {}

data "aws_region" "primary" {
  provider = aws.primary_region
}

data "aws_region" "secondary" {
  provider = aws.secondary_region
}

################################################################################
# S3 primary bucket
################################################################################

resource "aws_s3_bucket" "primary" {
  provider = aws.primary_region

  bucket = "tf-backend-${data.aws_caller_identity.current.account_id}-primary"

  tags = {
    "Name" = "tf-backend-${data.aws_caller_identity.current.account_id}-primary"
  }
}

resource "aws_s3_bucket_acl" "primary" {
  provider = aws.primary_region

  bucket = aws_s3_bucket.primary.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "primary" {
  provider = aws.primary_region

  bucket = aws_s3_bucket.primary.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "primary" {
  provider = aws.primary_region

  bucket = aws_s3_bucket.primary.id

  rule {
    bucket_key_enabled = true

    apply_server_side_encryption_by_default {
      sse_algorithm = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_replication_configuration" "primary" {
  provider = aws.primary_region

  role   = aws_iam_role.replication.arn
  bucket = aws_s3_bucket.primary.id

  rule {
    id       = "0"
    priority = "0"
    status   = "Enabled"

    source_selection_criteria {
      sse_kms_encrypted_objects {
        status = "Enabled"
      }
    }

    destination {
      bucket        = aws_s3_bucket.secondary.arn
      storage_class = "STANDARD"

      encryption_configuration {
        replica_kms_key_id = "arn:aws:kms:${data.aws_region.secondary.name}:${data.aws_caller_identity.current.account_id}:alias/aws/s3"
      }
    }
  }

  depends_on = [aws_s3_bucket_versioning.primary]
}

resource "aws_s3_bucket_public_access_block" "primary" {
  provider = aws.primary_region

  bucket = aws_s3_bucket.primary.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

################################################################################
# S3 secondary bucket
################################################################################

resource "aws_s3_bucket" "secondary" {
  provider = aws.secondary_region

  bucket = "tf-backend-${data.aws_caller_identity.current.account_id}-secondary"

  tags = {
    "Name" = "tf-backend-${data.aws_caller_identity.current.account_id}-secondary"
  }
}

resource "aws_s3_bucket_acl" "secondary" {
  provider = aws.secondary_region

  bucket = aws_s3_bucket.secondary.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "secondary" {
  provider = aws.secondary_region

  bucket = aws_s3_bucket.secondary.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "secondary" {
  provider = aws.secondary_region

  bucket = aws_s3_bucket.secondary.id

  rule {
    bucket_key_enabled = true

    apply_server_side_encryption_by_default {
      sse_algorithm = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "secondary" {
  provider = aws.secondary_region

  bucket = aws_s3_bucket.secondary.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

################################################################################
# IAM
################################################################################

resource "aws_iam_role" "replication" {
  name = "tf-backend-replication"

  assume_role_policy = jsonencode({
    Version : "2012-10-17",
    Statement : [
      {
        Action : "sts:AssumeRole",
        Effect : "Allow",
        Sid : ""
        Principal : {
          Service : "s3.amazonaws.com"
        },
      }
    ]
  })
}

resource "aws_iam_policy" "replication" {
  name = "tf-backend-replication-policy"

  policy = jsonencode({
    Version : "2012-10-17",
    Statement : [
      {
        Action : [
          "s3:GetReplicationConfiguration",
          "s3:ListBucket"
        ],
        Effect : "Allow",
        Resource : [
          "${aws_s3_bucket.primary.arn}"
        ]
      },
      {
        Action : [
          "s3:GetObjectVersionForReplication",
          "s3:GetObjectVersionAcl",
          "s3:GetObjectVersionTagging"
        ],
        Effect : "Allow",
        Resource : [
          "${aws_s3_bucket.primary.arn}/*"
        ]
      },
      {
        Action : [
          "s3:ReplicateObject",
          "s3:ReplicateDelete",
          "s3:ReplicateTags"
        ],
        Effect : "Allow",
        Resource : "${aws_s3_bucket.secondary.arn}/*"
      },
      {
        Action : ["kms:Decrypt"],
        Effect : "Allow",
        Resource : ["${aws_s3_bucket.primary.arn}"],
        Condition : {
          StringLike : {
            "kms:ViaService" : "s3.${data.aws_region.primary.name}.amazonaws.com",
            "kms:EncryptionContext:aws:s3:arn" : "${aws_s3_bucket.primary.arn}"
          }
        }
      },

      {
        Action : ["kms:Encrypt"],
        Effect : "Allow",
        Resource : ["${aws_s3_bucket.primary.arn}"],
        Condition : {
          "StringLike" : {
            "kms:ViaService" : "s3.${data.aws_region.primary.name}.amazonaws.com",
            "kms:EncryptionContext:aws:s3:arn" : "${aws_s3_bucket.primary.arn}"
          }
        }
      },
      {
        Action : ["kms:Encrypt"],
        Effect : "Allow",
        Resource : ["${aws_s3_bucket.secondary.arn}"],
        Condition : {
          "StringLike" : {
            "kms:ViaService" : "s3.${data.aws_region.secondary.name}.amazonaws.com",
            "kms:EncryptionContext:aws:s3:arn" : "${aws_s3_bucket.secondary.arn}"
          }
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "replication" {
  provider = aws.primary_region

  role       = aws_iam_role.replication.name
  policy_arn = aws_iam_policy.replication.arn
}

################################################################################
# DynamoDB tfstate lock tables
################################################################################

resource "aws_dynamodb_table" "this" {
  provider = aws.primary_region

  name             = "tf-backend-${data.aws_caller_identity.current.account_id}"
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = "LockID"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "LockID"
    type = "S"
  }

  replica {
    region_name = data.aws_region.secondary.name
  }

  tags = {
    "Name" = "tf-backend-${data.aws_caller_identity.current.account_id}"
  }
}
