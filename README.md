# tf-s3-backend

This module will create the resources needed for an S3 Terraform state backend. It will also create a configuration to replicate state files to a secondary bucket in another region.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.1.9 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.13 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.13 |
| <a name="provider_aws.primary_region"></a> [aws.primary\_region](#provider\_aws.primary\_region) | >= 4.13 |
| <a name="provider_aws.secondary_region"></a> [aws.secondary\_region](#provider\_aws.secondary\_region) | >= 4.13 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_iam_policy.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_s3_bucket.primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.secondary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_acl.primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | resource |
| [aws_s3_bucket_acl.secondary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl) | resource |
| [aws_s3_bucket_public_access_block.primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.secondary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_replication_configuration.primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_replication_configuration) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.secondary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_versioning.primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [aws_s3_bucket_versioning.secondary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_region.primary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_region.secondary](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dynamodb_lock_table_arn"></a> [dynamodb\_lock\_table\_arn](#output\_dynamodb\_lock\_table\_arn) | ARN of the DynamoDB tfstate lock table. |
| <a name="output_dynamodb_lock_table_id"></a> [dynamodb\_lock\_table\_id](#output\_dynamodb\_lock\_table\_id) | ID of the DynamoDB tfstate lock table. |
| <a name="output_primary_region"></a> [primary\_region](#output\_primary\_region) | Primary region where tfstate is located. |
| <a name="output_s3_primary_bucket_arn"></a> [s3\_primary\_bucket\_arn](#output\_s3\_primary\_bucket\_arn) | ARN of the primary tfstate S3 bucket. |
| <a name="output_s3_primary_bucket_id"></a> [s3\_primary\_bucket\_id](#output\_s3\_primary\_bucket\_id) | ID of the primary tfstate S3 bucket. |
| <a name="output_s3_secondary_bucket_arn"></a> [s3\_secondary\_bucket\_arn](#output\_s3\_secondary\_bucket\_arn) | ARN of the secondary tfstate S3 bucket. |
| <a name="output_s3_secondary_bucket_id"></a> [s3\_secondary\_bucket\_id](#output\_s3\_secondary\_bucket\_id) | ID of the secondary tfstate S3 bucket. |
| <a name="output_secondary_region"></a> [secondary\_region](#output\_secondary\_region) | Secondary region where replicated tfstate is located. |
